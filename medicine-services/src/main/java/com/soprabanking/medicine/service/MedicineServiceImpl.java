package com.soprabanking.medicine.service;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.soprabanking.medicine.exception.ResourceNotFoundException;
import com.soprabanking.medicine.model.Medicine;
import com.soprabanking.medicine.repository.MedicineRepo;
import com.soprabanking.medicine.util.GlobalConstants;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class MedicineServiceImpl implements MedicineService {

	// Adding default slf4j logging
	Logger log = LoggerFactory.getLogger(MedicineServiceImpl.class);
		
	@Autowired
	private MedicineRepo repository;

	@Autowired
	private SequenceGeneratorService sequenceGeneratorService;
	
	@Override
	public Mono<Medicine> save(Medicine medicine) {
		medicine.setId(sequenceGeneratorService.generateSequence(GlobalConstants.MEDICINE_SEQUENCE));
		log.info("Medicine: " + medicine.getMedicineName() + " has been added with id" + medicine.getId());
		return repository.save(medicine);
	}

	@Override
	public Flux<Medicine> findAll() {
		log.info("Medicine list is asked to be retrieved");
		return repository.findAll();
	}

	@Override
	public Mono<Medicine> getMedicine(int id) {
		log.info("Retrieving Medicine with id: " + id);
		return Mono.just(id)
				.flatMap(repository::findById)
				.switchIfEmpty(Mono.error(new ResourceNotFoundException("No medicine found for this id :: " + id)));
	}

	@Override
	public Mono<Medicine> updateMedicine(int id, Medicine medicine) {
		log.info("Update Medicine with id: " + id);
		Mono<Medicine> existingMedicine = repository.findById(id);
		if (Objects.isNull(existingMedicine))
			return Mono.error(new ResourceNotFoundException("No medicine found for this id :: " + id));
		return repository.save(medicine);
	}

	@Override
	public Mono<ResponseEntity<Void>> deleteMedicine(int id) {
		log.info("Delete Medicine with id: " + id);
		return repository.deleteById(id).then(Mono.just(new ResponseEntity<Void>(HttpStatus.OK)));
	}

}
