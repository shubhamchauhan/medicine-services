package com.soprabanking.medicine.service;

import org.springframework.http.ResponseEntity;

import com.soprabanking.medicine.model.Medicine;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MedicineService {

	Mono<Medicine> save(Medicine medicine);

	Flux<Medicine> findAll();

	Mono<Medicine> getMedicine(int id);

	Mono<Medicine> updateMedicine(int id, Medicine medicine);

	Mono<ResponseEntity<Void>> deleteMedicine(int id);
}
