package com.soprabanking.medicine.model;

import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.fasterxml.jackson.annotation.JsonFormat;

@Document(collection = "medicine")
public class Medicine {
	
	@Id
	private int id;
	
	@NotEmpty
	@Size(min = 3)
	private String medicineName;
	@NotEmpty
	@Size(min = 3)
	private String companyName;
	
	@NotNull
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
	private Date expiryDate;
	
	@Min(value=1, message="The amount must be equal or greater than INR 1")  
	private int amount;
	
	@Min(value=1, message="The quantity must be equal or greater than 1")  
	private int quantity;
	
	
	public Medicine(int id, String medicineName, String companyName, Date expiryDate, int amount, int quantity) {
		this.id = id;
		this.medicineName = medicineName;
		this.companyName = companyName;
		this.expiryDate = expiryDate;
		this.amount = amount;
		this.quantity = quantity;
	}
	
	public Medicine() {
		super();
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMedicineName() {
		return medicineName;
	}
	public void setMedicineName(String medicineName) {
		this.medicineName = medicineName;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public Date getExpiryDate() {
		return expiryDate;
	}
	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	@Override
	public String toString() {
		return "Medicine [id=" + id + ", medicineName=" + medicineName + ", companyName=" + companyName
				+ ", expiryDate=" + expiryDate + ", amount=" + amount + ", quantity=" + quantity + "]";
	}
	
}
