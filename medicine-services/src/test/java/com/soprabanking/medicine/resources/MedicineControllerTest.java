package com.soprabanking.medicine.resources;

import static org.mockito.Mockito.times;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;

import com.soprabanking.medicine.model.Medicine;
import com.soprabanking.medicine.model.MedicineInfoEvent;
import com.soprabanking.medicine.repository.MedicineRepo;
import com.soprabanking.medicine.service.MedicineServiceImpl;
import com.soprabanking.medicine.service.SequenceGeneratorService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@WebFluxTest(controllers = MedicineController.class)
@Import(MedicineServiceImpl.class)
class MedicineControllerTest {

	@Autowired
	private WebTestClient webClient;

	@MockBean
	private SequenceGeneratorService sequenceGeneratorService;

	@MockBean
	private MedicineRepo medicineRepo;
	
	@MockBean
	private KafkaTemplate<String, MedicineInfoEvent> kafkaTemplate;

	@Test
	void testFindAll() throws ParseException {
		
		Mockito.when(medicineRepo.findAll())
				.thenReturn(Flux.just(new Medicine(100, "Test Crocin 650", "GCK5 ", new SimpleDateFormat("yyyy-MM-dd").parse("2024-06-12"), 2, 200),
						new Medicine(101, "Test D-COLD TOTAL", "TY8 ", new SimpleDateFormat("yyyy-MM-dd").parse("2026-06-12"), 1, 400)));

		webClient.get().uri("/pharmacy/medicine/getAllMedicines")
				.exchange()
				.expectStatus()
				.isOk()
				.expectBodyList(Medicine.class);

		Mockito.verify(medicineRepo, times(1)).findAll();
	}
	 

	@Test
	void testGetMedicineById() throws ParseException {
		Medicine medicine = new Medicine();
		medicine.setId(100);
		medicine.setMedicineName("Test Crocin 650");
		medicine.setCompanyName("GSK5");
		medicine.setQuantity(10);
		medicine.setExpiryDate(new SimpleDateFormat("yyyy-MM-dd").parse("2024-06-12"));
		medicine.setAmount(20);

		Mockito.when(medicineRepo.findById(100)).thenReturn(Mono.just(medicine));

		webClient.get().uri("/pharmacy/medicine/getMedicine/{id}", 100)
				.exchange()
				.expectStatus()
				.isOk()
				.expectBody()
				.jsonPath("$.medicineName").isNotEmpty()
				.jsonPath("$.id").isEqualTo(100)
				.jsonPath("$.medicineName").isEqualTo("Test Crocin 650")
				.jsonPath("$.amount").isEqualTo(20);

		Mockito.verify(medicineRepo, times(1)).findById(100);
	}
	
	@Test
    void testCreateMedicine() throws ParseException {
		
		Date expirydate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2024-06-12 05:30:00");
		Medicine medicine = new Medicine();
		medicine.setId(200);
		medicine.setMedicineName("Test Crocin 650");
		medicine.setCompanyName("GSK5");
		medicine.setQuantity(10);
		medicine.setExpiryDate(expirydate);
		medicine.setAmount(20);
 
        Mockito.when(medicineRepo.save(medicine)).thenReturn(Mono.just(medicine));
 
        webClient.post()
            .uri("/pharmacy/medicine/addMedicine")
            .contentType(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromValue(medicine))
            .exchange()
            .expectStatus().isOk();
 
        //Mockito.verify(medicineRepo, times(1)).save(medicine);
    }

	
	@Test
	void testUpdateMedicine() throws ParseException {
		Medicine existingMedicine = new Medicine();
		existingMedicine.setId(100);
		existingMedicine.setMedicineName("Test Crocin 650");
		existingMedicine.setCompanyName("GSK5");
		existingMedicine.setQuantity(10);
		existingMedicine.setExpiryDate(new SimpleDateFormat("yyyy-MM-dd").parse("2024-06-12"));
		existingMedicine.setAmount(20);

		Medicine updatedMedicine = new Medicine();
		updatedMedicine.setId(100);
		updatedMedicine.setMedicineName("Test Crocin Ultra 650");
		updatedMedicine.setCompanyName("GSK5");
		updatedMedicine.setQuantity(100);
		updatedMedicine.setExpiryDate(new SimpleDateFormat("yyyy-MM-dd").parse("2024-06-12"));
		updatedMedicine.setAmount(18);

		Mockito.when(medicineRepo.findById(100)).thenReturn(Mono.just(existingMedicine));
		Mockito.when(medicineRepo.save(updatedMedicine)).thenReturn(Mono.just(updatedMedicine));

		webClient.put().uri("/pharmacy/medicine/editMedicine/{id}", 100).contentType(MediaType.APPLICATION_JSON)
				.body(BodyInserters.fromValue(updatedMedicine)).exchange().expectStatus().isOk();

		//Mockito.verify(medicineRepo, times(1)).findById(100);
		//Mockito.verify(medicineRepo, times(1)).save(updatedMedicine);
	}
	 
	
	@Test
	void testDeleteMedicine() {
		Mono<Void> voidReturn  = Mono.empty();
		
		Mockito
			.when(medicineRepo.deleteById(100))
			.thenReturn(voidReturn);
		
		webClient.delete()
			.uri("/pharmacy/medicine/deleteMedicine/{id}", 100)
			.exchange()
			.expectStatus()
			.isOk();
	}

	// @Test
		// void testSaveNewOrder() throws Exception {

		/*
		 * Medicine medicine = new Medicine(); medicine.setId(50);
		 * medicine.setMedicineName("Test Crocin 250"); medicine.setCompanyName("GSK1");
		 * //medicine.setExpiryDate(new Date("2020-06-12")); medicine.setQuantity(100);
		 * medicine.setAmount(2);
		 * 
		 * Order order = new Order(); order.setId(100);
		 * order.setMedicineName(medicine.getMedicineName());
		 * order.setOrderStatus(GlobalConstants.DEFAULT_ORDER_STATUS);
		 * order.setQuantity(10); order.setToAddress("10, Shanti Niketan, Meerut");
		 * order.setTotalAmount(20); order.setUserName("Shubham");
		 * 
		 * MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post(
		 * "/pharmacy/order/addNewOrder/{id}", medicine.getId())
		 * .content(order.toString()) .contentType(MediaType.APPLICATION_JSON)
		 * .accept(MediaType.APPLICATION_JSON)) .andExpect(status().isOk())
		 * .andReturn();
		 * 
		 * String resultSS = result.getResponse().getContentAsString();
		 * assertNotNull(resultSS); assertEquals(order.toString(), resultSS);
		 */
		/*
		 * when(orderService.save(50, order)).thenReturn(Mono.just(order));
		 * 
		 * webClient.post() .uri("/pharmacy/order/addNewOrder/{id}", 50)
		 * .contentType(MediaType.APPLICATION_JSON) .bodyValue(order)
		 * //(BodyInserters.fromValue(order)) .exchange() .expectBodyList(Order.class);
		 */

		// }


}
